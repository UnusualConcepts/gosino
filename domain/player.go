package domain

type Player struct {
	inGame bool
}

func (player Player) IsInGame() bool {
	return player.inGame
}

type Game struct {
}

func (player *Player) Join(game *Game) {
	player.inGame = true
}
