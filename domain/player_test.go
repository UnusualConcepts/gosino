package domain

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewPlayer_NotInGameByDefault(t *testing.T) {
	player := createPlayer()

	assert.False(t, player.IsInGame())
}

func TestPlayer_CanJoinGame(t *testing.T) {
	player := createPlayer()

	player.Join(createGame())

	assert.True(t, player.IsInGame())
}

func createGame() *Game {
	return &Game{}
}

func createPlayer() *Player {
	return &Player{}
}
